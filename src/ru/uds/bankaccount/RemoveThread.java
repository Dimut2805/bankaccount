package ru.uds.bankaccount;

public class RemoveThread extends Thread {
    Account account;

    RemoveThread(Account account) {
        this.account = account;
    }

    @Override
    public void run() {
        while (true) {
            account.removeMoney(1000);
        }
    }
}
