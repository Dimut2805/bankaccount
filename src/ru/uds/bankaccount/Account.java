package ru.uds.bankaccount;

public class Account {
    private long balance;

    public Account() {
        this(0);
    }

    public Account(long balance) {
        this.balance = balance;
    }

    public long getBalance() {
        return balance;
    }

    public synchronized void removeMoney(long money) {
        while (getBalance() < 1000) {
            try {
                wait();
            } catch (InterruptedException e) {
            }
        }
        checkMoney(money);
        this.balance -= money;
        System.out.println(balance);

    }

    public synchronized void addMoney(long money) {
        checkMoney(money);
        this.balance += money;
        System.out.println(balance);
        notify();
    }

    private void checkMoney(long money) {
        if (money < 0) {
            throw new ArithmeticException("Негативная сумма");
        }
    }
}