package ru.uds.bankaccount;

public class SalaryThread extends Thread {
    Account account;

    SalaryThread(Account account) {
        this.account = account;
    }

    @Override
    public void run() {
        for(int i = 0; i<20; i++) {
            account.addMoney(100);
            try {
                sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}