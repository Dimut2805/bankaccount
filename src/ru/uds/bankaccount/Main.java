package ru.uds.bankaccount;

import java.util.Scanner;

public class Main {
    private static Scanner in = new Scanner(System.in);

    public static void main(String[] args) {
        Account account = new Account(100);
        new SalaryThread(account).start();
        new RemoveThread(account).start();
    }

    private static void WorkingWithTheUser(Account account, int action) {
        switch (action) {
            case 1:
                account.addMoney(in.nextInt());
                break;
            case 2:
                account.removeMoney(in.nextInt());
                break;
            case 3:
                System.out.println("Ваш баланс: " + account.getBalance());
                break;
        }
    }
}